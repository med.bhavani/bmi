    function loadRecords(){
        
        for(var i=0;i<localStorage.length;i++){
            if(localStorage.key(i)=="record-counter")
                continue;//for getting record-counter values  to local storage skip the below commands and to get back to the loop (increaments)
            var obj=localStorage.getItem(localStorage.key(i));
            var objParsed=JSON.parse(obj);
            var temp=document.getElementsByTagName("template")[0];
            var clone = temp.content.cloneNode(true);
            // The cloneNode() method copies all attributes and their values.Tip: Use the appendChild() or insertBefore() method to insert the cloned node to the document.
        //query selector is similar to getelement by id
            clone.querySelector("#name").textContent=objParsed.Name;
            clone.querySelector("#age").textContent=objParsed.Age;
            clone.querySelector("#gender").textContent=objParsed.Gender;
            clone.querySelector("#weight").textContent=objParsed.Weight;
            clone.querySelector("#height").textContent=objParsed.Height;
            clone.querySelector("#FavouriteFood").textContent=objParsed.FavouriteFood;
            clone.querySelector("#bmi-text").textContent=objParsed.BMI;
            clone.querySelector("tr").setAttribute("id",objParsed.ID);
            // to get sequential order for the number of records in local storage
            document.getElementById("records-table").appendChild(clone);
            // parent child relation-ship for adding rows
        }
        
        var out="<button class=\"btn btn-danger\" onclick=\"clearRecords()\">Clear</button>";
        if(localStorage.length!=1){
            document.getElementById("content").innerHTML=out;
        }
        
    }
    function check(){
        if(localStorage.getItem("record-counter")==null){
            localStorage.setItem("record-counter","0");
        }
    }

    function clearall(){
        document.getElementById("bmiform").reset();
        document.getElementById("bmi-result").innerHTML="";

    }

    

    function clearRecords(){
        document.getElementById("records-table").innerHTML="";
        localStorage.clear();
        document.getElementById("content").innerHTML="";//template id
        localStorage.setItem("record-counter","0");
    }

    function update(obj){
        
        var name=obj.parentNode.parentNode.querySelectorAll("td")[0].innerHTML;
        var age=obj.parentNode.parentNode.querySelectorAll("td")[1].innerHTML;
        var gender=obj.parentNode.parentNode.querySelectorAll("td")[2].querySelector("#gender").value;//for showing current value 
        var weight=obj.parentNode.parentNode.querySelectorAll("td")[3].innerHTML;
        var height=obj.parentNode.parentNode.querySelectorAll("td")[4].innerHTML;
        var Favfood=obj.parentNode.parentNode.querySelectorAll("td")[5].innerHTML;
        var bmi=weight/Math.pow(height/100,2);
        // bmi=Math.round(bmi);
        var elid=obj.parentNode.parentNode.getAttribute("id");var isOk=true;
        if(name==""){
            obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#name").setAttribute("title","Name could not be empty.");
            isOk=false;    
        }else if(name.match(/^[A-Za-z" "]+$/)==null){
            obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#name").setAttribute("title","Name should contain only alphabets.");
            isOk=false;            
        }else{
            obj.parentNode.parentNode.querySelector("#name").setAttribute("class","when-no-error");
        }

        if(age==""){
            obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#age").setAttribute("title","Age could not be empty.");
            // innerHTML="*Age could not be empty.";
            isOk=false;

            //?
        }else if(parseInt(age)>120){
            console.log(age);
            obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#age").setAttribute("title","Age could not be greater than 120");
            // innerHTML="*Age could not be greater than 150.";
            isOk=false;
        }else if(age.match(/^[0-9]+$/)==null){
            obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#age").setAttribute("title","Age should contain only numbers");
            // innerHTML="*Age could not contain charecters other than numbers.";
            isOk=false;
        }else{
            obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("class","when-no-error");
            // document.getElementById("age-error-message").innerHTML="";
            obj.parentNode.parentNode.querySelector("#age").setAttribute("title"," ");
        }


        if(gender==null){
            obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#gender").setAttribute("title","Please select the gender");
            // innerHTML="*Please select the gender.";
            isOk=false;
        }else{
            obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("class","when-no-error");
            // document.getElementById("age-error-message").innerHTML="";
            obj.parentNode.parentNode.querySelector("#gender").setAttribute("title"," ");
            // document.getElementById("gender-error-message").innerHTML="";
        }

        if(weight==""){
            obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#weight").setAttribute("title","weight could not be empty.");
            //document.getElementById("weight-error-message").innerHTML="*weight could not be empty.";
            isOk=false;
        }
        else if(parseInt(weight)>200){
            obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#weight").setAttribute("title","weight could not be greater than 200");
            // innerHTML="*Age could not be greater than 150.";
            isOk=false;
        }
        //?
        else if(weight.match(/^[0-9]+$/)==null){
            obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#weight").setAttribute("title","weight should contain only numbers");
            //document.getElementById("weight-error-message").innerHTML="Weight should contain only numbers.";
            isOk=false;
        }else{
            //obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","when-no-error");
            obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","when-no-error");
            // document.getElementById("age-error-message").innerHTML="";
            obj.parentNode.parentNode.querySelector("#weight").setAttribute("title"," ");
             //document.getElementById("weight-error-message").innerHTML="";
        }
//?
        if(height==""){
            obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#height").setAttribute("title","height could not be empty.");
            //document.getElementById("height-error-message").innerHTML="height could not be empty.";
            isOk=false;
        }
        else if(parseInt(height)<24){
            obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#height").setAttribute("title","height could not be less than 24");
            //document.getElementById("height-error-message").innerHTML="Enter a valid height ";
            isOk=false;
        }
            else if(parseInt(height)>200){
                obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","when-error");
                obj.parentNode.parentNode.querySelector("#height").setAttribute("title","height could not be  greater than 200");
                //document.getElementById("height-error-message").innerHTML="Enter a valid height ";
                isOk=false;
        }
        else if(height.match(/^[0-9]+$/)==null){
            obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#height").setAttribute("title","height should contain only numbers");
            //document.getElementById("height-error-message").innerHTML="height should contain only numbers.";
            isOk=false;
        }
        else{
            obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","when-no-error");
            obj.parentNode.parentNode.querySelector("#height").setAttribute("title"," ");
            // document.getElementById("height-error-message").innerHTML="";
        }
        
        //?
        if(isFavFoodsOk==false){
            
            obj.parentNode.parentNode.querySelectorAll("td")[5].setAttribute("class","when-error");
            obj.parentNode.parentNode.querySelector("#fav-foods").setAttribute("title","Please select atleast one food item");
            //document.getElementById("fav-foods-error-message").innerHTML="*Please select atleast one food item.";
        }else{
            obj.parentNode.parentNode.querySelectorAll("td")[5].setAttribute("class","when-no-error");
            obj.parentNode.parentNode.querySelector("#fav-foods").setAttribute("title"," ");
            //document.getElementById("fav-foods-error-message").innerHTML="";
        }
    

//after updating the values of the rows and by clicking on the update get the rows in the readable form by removing the cursor  
        obj.parentNode.parentNode.querySelectorAll("td")[0].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[5].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#bmi-text").innerHTML=bmi;
        obj.parentNode.parentNode.querySelector("#bmi-text").setAttribute("style","display:inline");
        //for getting previous bgd color 
        obj.parentNode.parentNode.querySelectorAll("td")[0].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[5].setAttribute("class","");

        var out1="<button class=\" btn-primary\" id=\"b\" onclick=\"editRecord(this)\" style=\"text-align:center;\">Edit</button>";
        var out2="<button class=\" btn-primary\" id=\"b\" onclick=\"deleteRecord(this)\" style=\"text-align:center;\">Delete</button>";
        //after changing the values click on update .then we will get updated bmi value and the edit and  delete buttons in the place of update and cancel with the help of 6th index cell and 7th index cell from template tag. which is in previous-records.html page
        obj.parentNode.parentNode.querySelectorAll("td")[8].innerHTML=out2;
        obj.parentNode.parentNode.querySelectorAll("td")[7].innerHTML=out1;
       
        

        var updateObj=localStorage.getItem(elid);
        var parsedUpdateObj=JSON.parse(updateObj);
        
        parsedUpdateObj.Name=name;
        parsedUpdateObj.Age=age;
        parsedUpdateObj.Gender=gender;
        parsedUpdateObj.Weight=weight;
        parsedUpdateObj.Height=height;
        parsedUpdateObj.FavouriteFood=FavouriteFood;
        parsedUpdateObj.BMI=bmi;

        var stringifiedUpdateObj=JSON.stringify(parsedUpdateObj);
        localStorage.setItem(elid,stringifiedUpdateObj);
    }
    function deleteRecord(obj){
        document.getElementById("records-table").deleteRow(obj.parentNode.parentNode.rowIndex-1);
        localStorage.removeItem(obj.parentNode.parentNode.getAttribute("id"));
    }
    function cancel(obj){
        obj.parentNode.parentNode.querySelectorAll("td")[0].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelectorAll("td")[5].setAttribute("contenteditable","false");
        obj.parentNode.parentNode.querySelector("#bmi-text").setAttribute("style","display:inine");//when we click on edit bmi will gets disappear so after editing the values we get the bmi for edited values and for appearing the bmi we use inline display
        //
        obj.parentNode.parentNode.querySelectorAll("td")[0].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","");
        obj.parentNode.parentNode.querySelectorAll("td")[5].setAttribute("class","");

        var out1="<button class=\"btn btn-primary\" id=\"b\" onclick=\"editRecord(this)\" style=\"text-align:center;\">Edit</button>";
        var out2="<button class=\"btn btn-primary\" id=\"b\" onclick=\"deleteRecord(this)\" style=\"text-align:center;\">Delete</button>";
        obj.parentNode.parentNode.querySelectorAll("td")[7].innerHTML=out1;
        obj.parentNode.parentNode.querySelectorAll("td")[8].innerHTML=out2;
        
    }

    function editRecord(obj){
        obj.parentNode.parentNode.querySelectorAll("td")[0].setAttribute("contenteditable","true");
        obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("contenteditable","true");
        obj.parentNode.parentNode.querySelectorAll("td")[2].innerHTML="";
        obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("contenteditable","true");
        obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("contenteditable","true");

        var curVal=document.getElementById("#gender");
var temp=document.getElementById("gender-template");
console.log(temp);
var clone = temp.content.cloneNode(true);
if(clone.getElementById("male").value==curVal){
    clone.getElementById("male").selected=true;
}else if(temp.content.getElementById("female").value==curVal){
    clone.getElementById("female").selected=true;
}
        obj.parentNode.parentNode.querySelectorAll("td")[5].innerHTML="";
        obj.parentNode.parentNode.querySelectorAll("td")[2].appendChild(clone);
        obj.parentNode.parentNode.querySelector("#bmi-text").setAttribute("style","display:none;");
        obj.parentNode.parentNode.querySelectorAll("td")[0].setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelectorAll("td")[1].setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelectorAll("td")[2].setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelectorAll("td")[3].setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelectorAll("td")[4].setAttribute("class","when-editing");
        obj.parentNode.parentNode.querySelectorAll("td")[5].setAttribute("class","when-editing");
        
        var out1="<button class=\" btn-success\" id=\"b\" onclick=\"update(this)\" style=\"text-align:center;\">Update</button>";
        var out2="<button class=\" btn-danger\" id=\"b\" onclick=\"cancel(this)\" style=\"text-align:center;\">Cancel</button>";
        var elid=obj.parentNode.parentNode.getAttribute("id");

        var tag = obj.parentNode.parentNode.cells[0]; //for getting cursor at the end of the content after entering the value from the user
            var l=tag.childNodes[0].length;
            // Creates range object 
            var setpos = document.createRange(); 
              
            // Creates object for selection 
            var set = window.getSelection(); 
              
            // Set start position of range 
            setpos.setStart(tag.childNodes[0], l); 
              
            // Collapse range within its boundary points 
            // Returns boolean 
            setpos.collapse(true); 
              
            // Remove all ranges set 
            set.removeAllRanges(); 
              
            // Add range with respect to range object. 
            set.addRange(setpos); 
              
            // Set cursor on focus 
            tag.focus();

        obj.parentNode.parentNode.querySelectorAll("td")[8].innerHTML=out2;
        obj.parentNode.parentNode.querySelectorAll("td")[7].innerHTML=out1;
        
        
    }

    function calculateBMI(){
        allLetter(bmiform.name);
        checkAge(bmiform.age);
        checkGender();
        wgt();
        food();
        if(localStorage.getItem("record-counter")==null){
            localStorage.setItem("record-counter","0");
        }
        
        var name=document.getElementById("name").value;
        var age=document.getElementById("age").value;
        var rb1=document.getElementById("male");
        var rb2=document.getElementById("female");
        var gender;
        if(rb1.checked){
            gender=rb1.value;
        }else if(rb2.checked){
            gender=rb2.value;
        }
        var weight=document.getElementById("weight").value;
        var height=document.getElementById("height").value;
        // varfavfood=document.getElementById("favfood").value;
        var cb1=document.getElementById("Chinese");
        var cb2=document.getElementById("Indian");
        var cb3=document.getElementById("Italian");
        var FavouriteFood;
        if(cb1.checked){
            FavouriteFood=cb1.value;
        }else if(cb2.checked){
            FavouriteFood=cb2.value;
        }
        else{
            FavouriteFood=cb3.value
        }
        
        bmi = (document.getElementById("weight").value)/(Math.pow((document.getElementById("height").value/100),2));
       // bmi = Math.round(bmi);

        var rc=localStorage.getItem("record-counter");
        var irc=parseInt(rc);
        irc++;
        var src=irc.toString();
        localStorage.setItem("record-counter",src);
        var obj={
            ID:src,
            Name:name,
            Age:age,
            Gender:gender,
            Weight:weight,
            Height:height,
            FavouriteFood:FavouriteFood,
            BMI:bmi
        }

        objStringified=JSON.stringify(obj);

        localStorage.setItem(src,objStringified);
        document.getElementById("bmi-result").innerHTML=bmi;

        
        
    };

  //////////////////////////////////////////////////////////////////////////////////////////////////
    



    function allLetter(val)
       {
        //    foc=document.getElementById("name");
             var lets = /^[A-Za-z]+$/;
                if(val.value.match(lets))
                    {
    return 1;
        }
    else
        {
    //alert(" enter a valid name containing only alphabets...");
    //foc.focus();
    document.getElementById("name").focus();
               document.getElementById("name").style.borderColor="red";
          document.getElementById("name").style.borderStyle="solid";
	        //  alert("please Enter only characters");                                      
	        
// console.log(bmiform.name.value);
       
          }
    }



    function checkAge(){
    iage = document.getElementById("age").value;
    var lettrs=/^[0-9]+$/;
    if(iage.match(lettrs))
         {
    return 1;
         }
    else 
         {
    //alert("please Enter a valid age");
    // lettrs.focus();
    document.getElementById("age").focus();
               document.getElementById("age").style.borderColor="red";
          document.getElementById("age").style.borderStyle="solid";
	                
         }
       }


   
    
function wgt(){
  height= document.getElementById("height").value;
    weight=document.getElementById("weight").value;
    // console.log(height);
    // console.log(weight);

    if(weight > 0 && height > 0)
                 { 
        //    alert(12313213);
       finalBmi = weight/((height/100)*(height/100));
       document.getElementById("bmi-result").innerHTML=finalBmi;
       }else{
        //alert("Please Fill in everything correctly");
        // height.focus();
        // weight.focus();
        document.getElementById("weight").focus();
               document.getElementById("weight").style.borderColor="red";
          document.getElementById("weight").style.borderStyle="solid";
            // alert("please Enter the weight ");       
             document.getElementById("height").focus();
               document.getElementById("height").style.borderColor="red";
          document.getElementById("height").style.borderStyle="solid";
	         //alert("please Enter the height");       
                 }
}
     


function checkGender()
   {
     if(document.getElementById("male").checked)
     {
       return 1;
 
     }
     else if(document.getElementById("female").checked)
     {
       return 1;
     }
     else
     {
       //alert("please select your gender ")
     }
   }
   function food()
   {
     if(document.getElementById("Chinese").checked)
     {
       return 1;
     }
     else if(document.getElementById("Indian").checked)
     {
       return 1;
     }
     else if(document.getElementById("Italian").checked)
     {
       return 1;
     }
     else
     {
       alert("Mention atleast one of your favourite food...");
     }
   }
       
       
   //////////////////////////////
    




   

function checkName() {
    var x = document.getElementById("name").value;
    if(x == ""){
        document.getElementById("nameError").style.display = "block";
    }else{
        document.getElementById("nameError").style.display = "none";
    }
}
 
function checkName2() {
    var x2 = document.getElementById("age").value;
    if(x2==""||x2<0||x2 >120){
        document.getElementById("nameError2").style.display = "block";
    }else{
        document.getElementById("nameError2").style.display = "none";
    }
}
function checkName3() {
    var x3 = document.getElementById("weight").value;
    if(x3 == ""){
        document.getElementById("nameError3").style.display = "block";
    }else{
        document.getElementById("nameError3").style.display = "none";
    }
}
function checkName4() {
    var x4 = document.getElementById("height").value;
    if(x4 == ""){
        document.getElementById("nameError4").style.display = "block";
    }else{
        document.getElementById("nameError4").style.display = "none";
    }
}
 

